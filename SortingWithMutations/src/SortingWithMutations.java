import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Timer;

import sortingHelpers.BubbleSortWithMutations;
import sortingHelpers.HeapSortWithMutations;
import sortingHelpers.InsertionSort;
import sortingHelpers.ListMutation;
import sortingHelpers.MergeSort;
import sortingHelpers.QuickSort;
import sortingHelpers.QuickSortWithMutations;
import sortingHelpers.SortingMethods;

public class SortingWithMutations {

	/**
	 * List of Intergers used to create a Random Uniformly Distributed Set
	 */
	private static volatile List<Integer> mutatingList;
	private static volatile int sortRank;

	/**
	 * Constructor used to create List of Integers that are Uniformly Distibuted
	 * then Randomized
	 * 
	 * @param int size
	 * @param int maxNumber
	 * @return List<Integers> clone/copy, not a reference
	 */
	public static List<Integer> RandomSample(int size, int maxNumber) {
		mutatingList = new ArrayList<Integer>();
		for (int i = 0; i < size; i++) {
			mutatingList.add(new Integer(i + 1));
		}
		RandomizeList();
		return Clone(mutatingList);
	}

	public static List<Integer> GetMutatingList() {
		return mutatingList;
	}

	public static List<Integer> Clone(List<Integer> aList) {
		List<Integer> clone = new ArrayList<Integer>(aList.size());
		for (Integer item : aList) clone.add(item);
		return clone;
	}

	/**
	 * main
	 * 
	 * @param String
	 *            
	 */
	/**
	 * @param args
	 */
	/**
	 * @param args
	 */
	/**
	 * @param args
	 */
	/**
	 * @param args
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		int m = 5000;
		int n = 5000;
		System.out.println(Arrays.toString(RandomSample(m, n).toArray()));
		SortingMethods listCopy = new MergeSort(mutatingList);
		System.out.println(String.format("Originial Inversion Rank %d",
				listCopy.GetListInversionRanking()));
		
		MergeSort ourMergeSort = new MergeSort();
		// System.out.println(String.format("\nAfter Merge Sort %s",
		// Arrays.toString(ourMergeSort.Sort(RandomSample.GetListToRandomize()).toArray())));
		ourMergeSort.Sort(SortingWithMutations.Clone(mutatingList));
		System.out.println(String.format("MergeSort Comparisons %d",
				ourMergeSort.GetSortRank()));
		System.out.println(String.format("Inversion Ranks is now equal to %d",
				ourMergeSort.GetListInversionRanking()));
		
		InsertionSort ourInsertSort = new InsertionSort();
		// System.out.println(String.format("\nAfter Insert Sort %s",
		// Arrays.toString(ourInsertSort.Sort(RandomSample.GetListToRandomize()).toArray())));
		ourInsertSort.Sort(SortingWithMutations.Clone(mutatingList));
		System.out.println(String.format("InsertSort Comparisons %d",
				ourInsertSort.GetSortRank()));
		System.out.println(String.format("Inversion Ranks is now equal to %d",
				ourInsertSort.GetListInversionRanking()));

		QuickSort ourQuickSort = new QuickSort();
		// System.out.println(String.format("\nAfter Quick Sort %s",
		// Arrays.toString(ourQuickSort.Sort(RandomSample.GetListToRandomize()).toArray())));
		ourQuickSort.Sort(SortingWithMutations.Clone(mutatingList));
		System.out.println(String.format("QuickSort Comparisons %d",
				ourQuickSort.GetSortRank()));
		System.out.println(String.format("Inversion Ranks is now equal to %d",
				ourQuickSort.GetListInversionRanking()));

		
		System.out.println("Starting HeapSortWithMutations");
		HeapSortWithMutations ourHSMutation = new HeapSortWithMutations();
		ourHSMutation.Sort(SortingWithMutations.Clone(mutatingList));
		System.out.println(String.format("HeapSortWithMutations Comparisons %d",
				ourHSMutation.GetSortRank()));
		System.out.println(String.format("Inversion Ranks is now equal to %d",
				ourHSMutation.GetListInversionRanking()));
		System.out.println(String.format("Percent Sorted based on Inversions %.2f%%",
				SortingMethods.GetPercentSorted(listCopy.GetListInversionRanking(),
						ourHSMutation.GetListInversionRanking())));
		System.out.println(String.format("Approved Mutations: %d",
				ListMutation.approved));
		System.out.println(String.format("Denied Mutations: %d",
				ListMutation.denied));
		System.out.println(String.format("Percent Approved Mutations: %.0f%%",
				(double)ListMutation.approved / (double)ListMutation.denied * 100.0));
		
		System.out.println("Starting BubbleSortWithMutations");
		BubbleSortWithMutations ourBSMutation = new BubbleSortWithMutations();
		ourBSMutation.Sort(SortingWithMutations.Clone(mutatingList));
		System.out.println(String.format("BubbleSortWithMutations Comparisons %d",
				ourBSMutation.GetSortRank()));
		System.out.println(String.format("Inversion Ranks is now equal to %d",
				ourBSMutation.GetListInversionRanking()));
		System.out.println(String.format("Percent Sorted based on Inversions %.2f%%", 
				SortingMethods.GetPercentSorted(listCopy.GetListInversionRanking(),
												ourBSMutation.GetListInversionRanking())));
		System.out.println(String.format("Approved Mutations: %d",
				ListMutation.approved));
		System.out.println(String.format("Denied Mutations: %d",
				ListMutation.denied));
		System.out.println(String.format("Percent Approved Mutations: %.2f%%",
				(double)ListMutation.approved / (double)ListMutation.denied * 100.0));
		
		
//		System.out.println("Starting BubbleSortWithMutations");
//		QuickSortWithMutations ourQSMutation = new QuickSortWithMutations();
//		ourQSMutation.Sort(SortingWithMutations.Clone(mutatingList));
//		System.out.println(String.format("QuickSortWithMutations Comparisons %d",
//				ourQSMutation.GetSortRank()));
//		System.out.println(String.format("Inversion Ranks is now equal to %d",
//				ourQSMutation.GetListInversionRanking()));
	}

	/**
	 * Randomize the Uniformly Distributed List
	 * 
	 * @param none
	 * @return none
	 * @see RandomSample
	 */
	static void RandomizeList()
	{
		Random randItem = new Random();
		for (int i = 0; i < mutatingList.size(); i++)
		{
			swap(i, randItem.nextInt(mutatingList.size()));
		}
	}

	/**
	 * Swap 2 items in the list
	 * 
	 * @param int pos1
	 * @param int pos2
	 */
	private static void swap(int pos1, int pos2)
	{
		Collections.swap(mutatingList, pos1, pos2);
	}
	
}
