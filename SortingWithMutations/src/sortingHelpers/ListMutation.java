package sortingHelpers;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class ListMutation {
	
	static int r;	
	public static int approved;
	public static int denied;
	public static int pos1;
	public static int pos2;
//	static List<Integer> randRList;
//	static int rPosition;	
//	
//	private static void setUpRandList()
//	{
//		randRList = new ArrayList<Integer>();
//		Random randomObj = new Random();
//		int value = -1;
//		if(randRList.size() != 0)
//			randRList.clear();
//		for(int i = 0; i <= rPosition; i++)
//		{
//			do
//			{
//				value = randomObj.nextInt(10);
//			}
//			while(randRList.contains(value));
//			randRList.add(value);
//			rPosition = rPosition == 0 ? 1 : rPosition == 1 ? 2 : 0;
//		}
//	}
//	
//	private static boolean mutationApproved()
//	{
//		setUpRandList();
//		for(int i = 0; i < randRList.size(); i++)
//		{
//			if(randRList.contains(new Random().nextInt(10)))
//			{
//				return true;
//			}
//		}
//		return false;
//	}

	private static boolean mutationApproved()
	{
		double randValue = Math.random();
		switch(r)
		{
			case 0:
				r++;
				return randValue < 0.1;
			case 1:
				r++;
				return randValue < 0.2;
			case 2:
				r = 0;
				return randValue < 0.3;
			default:
				return false;
		}
	}
	
	public static boolean mutateTwoItems(List<Integer> mutatingList)
	{
		if(mutationApproved())
		{
			approved++;
			pos1 = new Random().nextInt(mutatingList.size());
			pos2 = new Random().nextInt(mutatingList.size());
			Collections.swap(mutatingList, pos1, pos2);
			return true;
		}
		denied++;
		return false;
	}
	
	public static boolean timeToStopSorting(long startTime, int maxMillis)
	{
		return System.currentTimeMillis() - startTime > maxMillis;
	}

}
