package sortingHelpers;
import java.util.List;

/**
 * BubbleSortWithMutations class to test how bubble sort perfoms with
 * a mutating list with revolving percent mutations of 10, 20, & 30%
 * 
 */
public class BubbleSortWithMutations extends SortingMethods
{
	final static int TIME_LIMIT = 1000 * 60 * 2;
	static long startTime;
	
	/**
	 * 
	 */
	public BubbleSortWithMutations()
	{
		super();
	}
	/**
	 * @param sortRank
	 */
	public BubbleSortWithMutations(int sortRank)
	{
		super(sortRank);
	}
	/**
	 * @param listToSort
	 */
	public BubbleSortWithMutations(List<Integer> listToSort)
	{
		super(listToSort);
	}
	/**
	 * @param listToSort
	 * @param sortRank
	 */
	public BubbleSortWithMutations(List<Integer> listToSort, int sortRank)
	{
		super(listToSort, sortRank);
	}
	
	/**
	 * @param listToSort
	 * @return
	 */
	public List<Integer> Sort(List<Integer> listToSort)
	{
		startTime = System.currentTimeMillis();
		this.listToSort = listToSort;
		do
		{
			bubbleSort(listToSort);
		}while(!ListMutation.timeToStopSorting(startTime, TIME_LIMIT));
		return GetListToSort();
	}
	
	/**
	 * @param listToSort
	 */
	private void bubbleSort(List<Integer> listToSort)
	{
        for(int i = 0; i < listToSort.size() - 1; i++)
        {
            for(int j = 1; j < listToSort.size() - i; j++)
            {
            	if(ListMutation.timeToStopSorting(startTime, TIME_LIMIT))
            		return;
            	ListMutation.mutateTwoItems(listToSort);
            		
            	incrementSortRank();
            	if(listToSort.get(j - 1) > listToSort.get(j))
                {
                    swap(listToSort, j - 1, j);
                }
            }
        }
	}
}
