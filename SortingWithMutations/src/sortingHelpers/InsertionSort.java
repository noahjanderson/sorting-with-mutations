package sortingHelpers;
import java.util.List;

public class InsertionSort extends SortingMethods
{
	public InsertionSort()
	{
		super();
	}
	
	public InsertionSort(int sortRank)
	{
		super(sortRank);
	}	
	
	public InsertionSort(List<Integer> listToSort)
	{
		super(listToSort);
	}	
	
	public InsertionSort(List<Integer> listToSort, int sortRank)
	{
		super(listToSort, sortRank);
	}	
	
	
	public List<Integer> Sort(List<Integer> listToSort)
	{
		this.listToSort = listToSort;
		this.helperList = GetListToSort();
		insertionSort();
		return GetListToSort();
	}
	
	private void insertionSort()
	{
		int j;
		int currentValue;
		int i;

		for (j = 1; j < listToSort.size(); j++)
		{
			currentValue = listToSort.get(j);
			for (i = j - 1; (i >= 0) && (listToSort.get(i) > currentValue); i--)
			{
				incrementSortRank();
				listToSort.set(i + 1, listToSort.get(i));
			}
			listToSort.set(i + 1, currentValue);
		}
	}
}
