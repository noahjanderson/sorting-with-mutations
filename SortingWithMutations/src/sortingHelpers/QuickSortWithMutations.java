package sortingHelpers;
import java.util.List;

public class QuickSortWithMutations extends SortingMethods
{
	final static int TIME_LIMIT = 1000 * 60;
	static long startTime; 
	
	public QuickSortWithMutations()
	{
		super();
	}
	
	public QuickSortWithMutations(int sortRank)
	{
		super(sortRank);
	}
	
	public QuickSortWithMutations(List<Integer> listToSort)
	{
		super(listToSort);
	}
	
	public List<Integer> Sort(List<Integer> listToSort)
	{
		startTime = System.currentTimeMillis();
        this.listToSort = listToSort;
        do
        {
        	quickSort(listToSort, 0, listToSort.size() - 1);
        }while(!ListMutation.timeToStopSorting(startTime, TIME_LIMIT));
        return GetListToSort();
    }
 
    private void quickSort(List<Integer> listToSort, int lowerIndex, int higherIndex)
    {
        
        int i = lowerIndex;
        int j = higherIndex;
        int pivotVal = choosePivotValue(lowerIndex, higherIndex);
        while (i <= j)
        {
        	
            while (listToSort.get(i) < pivotVal)
            {
            	if(ListMutation.timeToStopSorting(startTime, TIME_LIMIT))
            		return;
            	
            	if(ListMutation.mutateTwoItems(listToSort))
            		return;
            	incrementSortRank();
                i++;
            }
            while (listToSort.get(j) > pivotVal)
            {
            	if(ListMutation.timeToStopSorting(startTime, TIME_LIMIT))
            		return;
            	ListMutation.mutateTwoItems(listToSort);
            	incrementSortRank();
                j--;
            }
            if (i <= j)
            {
                swap(listToSort, i, j);
                i++;
                j--;
            }
        }
        if (lowerIndex < j)
        {
        	quickSort(listToSort, lowerIndex, j);
        }
        if (i < higherIndex)
        {
            quickSort(listToSort, i, higherIndex);
        }
    }

	private int choosePivotValue(int lowerIndex, int higherIndex) 
	{
		return listToSort.get(lowerIndex + (higherIndex - lowerIndex) / 2);
	}
}
