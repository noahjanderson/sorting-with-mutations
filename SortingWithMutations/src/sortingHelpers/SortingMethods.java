package sortingHelpers;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortingMethods {
	
	protected List<Integer> listToSort;
	protected List<Integer> helperList;
	protected int sortRank;
	protected boolean interruptSort;
	
	public SortingMethods()
	{
		listToSort = new ArrayList<Integer>();
		helperList = new ArrayList<Integer>();
		interruptSort = false;
	}
	
	public SortingMethods(int sortRank)
	{
		super();
		this.sortRank = sortRank;
	}

	public SortingMethods(List<Integer> listToSort)
	{
		super();
		this.listToSort = listToSort; 
	}
	
	public SortingMethods(List<Integer> listToSort, int sortRank)
	{
		super();
		this.sortRank = sortRank;
		this.listToSort = listToSort;
	}
	
	public List<Integer> GetListToSort()
	{
		List<Integer> clone = new ArrayList<Integer>(listToSort.size());
		for (Integer item : listToSort) clone.add(item);
		return clone;
	}
	
	public List<Integer> CloneToHelperList(int value)
	{
		List<Integer> clone = new ArrayList<Integer>(listToSort.size());
		for (Integer item : listToSort) clone.add(value);
		return clone;
	}
	

	/**
	 * Determine the Ranking of List by amount of its inversions
	 * 
	 * @param none
	 * @return integer value of the ranking of inversions
	 */
	public int GetListInversionRanking()
	{
		int rank = 0;
		for (int i = 0; i < listToSort.size(); i++)
		{
			for (int j = i + 1; j < listToSort.size(); j++)
			{
				rank += listToSort.get(i) > listToSort.get(j) ? 1 : 0;
			}
		}
		return rank;
	}
	
	
	public int GetSortRank()
	{
		return sortRank;
	}
	
	public static double GetPercentSorted(int originalInversions, int finalInversions)
	{
		return (double)(originalInversions - finalInversions) / (double)originalInversions * 100.0;
	}
	
	protected void incrementSortRank()
	{
		sortRank++;
	}
	
	protected void swap(List<Integer> listToSort, int pos1,int pos2)
	{
		Collections.swap(listToSort, pos1, pos2);
	}

}
