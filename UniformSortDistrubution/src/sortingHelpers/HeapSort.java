package sortingHelpers;
import java.util.List;

public class HeapSort extends SortingMethods
{
	private int N;
	
	public HeapSort()
	{
		super();
	}
	
	public HeapSort(int sortRank)
	{
		super(sortRank);
	}
	public List<Integer> Sort(List<Integer> listToSort)
    {       
		this.listToSort = listToSort;
        heapify(listToSort);        
        for (int i = N; i > 0; i--)
        {
            swap(listToSort,0, i);
            N = N - 1;
            maxheap(listToSort, 0);
        }
        return GetListToSort();
    }     
    /* Function to build a heap */   
    public void heapify(List<Integer> listToSort)
    {
        N = listToSort.size() - 1;
        for (int i = N / 2; i >= 0; i--)
        {
            maxheap(listToSort, i);
        }
    }
    /* Function to swap largest element in heap */        
    public void maxheap(List<Integer> listToSort, int largestValPos)
    { 
        int left = 2 * largestValPos ;
        int right = 2 * largestValPos + 1;
        int max = largestValPos;
        incrementSortRank();
        if (left <= N && listToSort.get(left) > listToSort.get(largestValPos))
        {
        	max = left;
        }
        incrementSortRank();
        if (right <= N && listToSort.get(right) > listToSort.get(max))        
        {
        	max = right;
        }
 
        if (max != largestValPos)
        {
            swap(listToSort, largestValPos, max);
            maxheap(listToSort, max);
        }
    }      
}
