package sortingHelpers;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortingMethods {
	
	protected List<Integer> listToSort;
	protected List<Integer> helperList;
	protected int sortRank;
	
	public SortingMethods()
	{
		listToSort = new ArrayList<Integer>();
		helperList = new ArrayList<Integer>();
	}
	
	public SortingMethods(int sortRank)
	{
		super();
		this.sortRank = sortRank;
	}
	
	public SortingMethods(List<Integer> listToSort)
	{
		super();
		listToSort = this.listToSort;
	}

	public List<Integer> GetListToSort()
	{
		List<Integer> clone = new ArrayList<Integer>(listToSort.size());
		for (Integer item : listToSort) clone.add(item);
		return clone;
	}
	
	public List<Integer> SetAllHelperElements(int value)
	{
		List<Integer> clone = new ArrayList<Integer>(listToSort.size());
		for (Integer item : listToSort) clone.add(value);
		return clone;
	}
	
	public int GetSortRank()
	{
		return sortRank;
	}
	
	protected void incrementSortRank()
	{
		sortRank++;
	}
	
	protected void swap(List<Integer> listToSort, int pos1,int pos2)
	{
		Collections.swap(listToSort, pos1, pos2);
	}

}
