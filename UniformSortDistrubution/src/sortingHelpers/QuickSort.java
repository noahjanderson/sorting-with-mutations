package sortingHelpers;
import java.util.List;

public class QuickSort extends SortingMethods
{
	public QuickSort()
	{
		super();
	}
	
	public QuickSort(int sortRank)
	{
		super(sortRank);
	}
	
	public List<Integer> Sort(List<Integer> listToSort)
	{
        this.listToSort = listToSort;
        quickSort(listToSort, 0, listToSort.size() - 1);
        return GetListToSort();
    }
 
    private void quickSort(List<Integer> listToSort, int lowerIndex, int higherIndex) {
         
        int i = lowerIndex;
        int j = higherIndex;
        int pivotVal = choosePivotValue(lowerIndex, higherIndex);
        while (i <= j)
        {
            while (listToSort.get(i) < pivotVal)
            {
            	incrementSortRank();
                i++;
            }
            while (listToSort.get(j) > pivotVal)
            {
            	incrementSortRank();
                j--;
            }
            if (i <= j)
            {
                swap(listToSort, i, j);
                i++;
                j--;
            }
        }
        if (lowerIndex < j)
        {
        	quickSort(listToSort, lowerIndex, j);
        }
        if (i < higherIndex)
        {
            quickSort(listToSort, i, higherIndex);
        }
    }

	private int choosePivotValue(int lowerIndex, int higherIndex) 
	{
		return listToSort.get(lowerIndex + (higherIndex - lowerIndex) / 2);
	}
}
