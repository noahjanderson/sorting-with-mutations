package sortingHelpers;

import java.util.List;

public class RadixSort extends SortingMethods
{
	public RadixSort()
	{
		super();
	}
	public RadixSort(int sortRank)
	{
		super(sortRank);
	}
	
	public List<Integer> Sort(List<Integer> listToSort)
    {
		Integer[] tempList = new Integer[listToSort.size()];
        for(int i = 0; i < listToSort.size(); i++)
        {
        	tempList[i] = new Integer(listToSort.get(i));
        }
        radixSort(tempList);
        return GetListToSort();
    }
	
	private void radixSort(Integer [] tempList)
	{
		int i;
        int m = tempList[0];
        int exp = 1;
        int n = tempList.length;
        int[] b = new int[10];
        for (i = 1; i < n; i++)
            if (tempList[i] > m)
                m = tempList[i];
        while (m / exp > 0)
        {
            int[] bucket = new int[10];
 
            for (i = 0; i < n; i++)
                bucket[(tempList[i] / exp) % 10]++;
            for (i = 1; i < 10; i++)
                bucket[i] += bucket[i - 1];
            for (i = n - 1; i >= 0; i--)
                b[--bucket[(tempList[i] / exp) % 10]] = tempList[i];
            for (i = 0; i < n; i++)
                tempList[i] = b[i];
            exp *= 10;        
        }
        for(int j = 0; j < tempList.length; j++)
        {
        	listToSort.set(j, tempList[j]);
        }   
    }    
}
