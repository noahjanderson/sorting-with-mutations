package sortingHelpers;
import java.util.List;

public class IntroSort extends SortingMethods 
{
	private static int SIZE_THRESHOLD = 16;
	
	public IntroSort()
	{		
		super();
	}
	
	public IntroSort(int sortRank)
	{
		super(sortRank);
	}
	
	public List<Integer> Sort(List<Integer> listToSort)
	{
		this.listToSort = listToSort;
		int maxDepth = (int) (Math.floor(Math.log(listToSort.size())) * 2);
		introSort(listToSort, 0, listToSort.size() - 1, maxDepth);
		return GetListToSort();
	}
	

	
	private void introSort(List<Integer> listToSort, int lowIndex, int highIndex, int maxDepth)
	{
		int listSize = listToSort.size();
		int partitionPosition = 0;
		while(highIndex - lowIndex > SIZE_THRESHOLD)
		{

			if(listSize <= 1)
			{
				return;
			}
			else if (maxDepth == 0)
			{
				HeapSort ourHeapSort = new HeapSort(sortRank);
				listToSort = ourHeapSort.Sort(listToSort);
				sortRank = ourHeapSort.sortRank;
			}
			else
			{
				partitionPosition = partition(listToSort, lowIndex, highIndex);
				introSort(listToSort, partitionPosition, highIndex, maxDepth - 1);
			}
		}
		InsertionSort ourInsertSort = new InsertionSort(sortRank);
		
	}
	
	private int partition(List<Integer> listToSort, int lowPosIndex, int highPosIndex)
	{
		int pivotIndex = choosePivot(listToSort, lowPosIndex, highPosIndex);
		int pivotVal = listToSort.get(pivotIndex);
		swap(listToSort, pivotIndex, highPosIndex);
		int finalPosIndex = lowPosIndex;
		for(int i = lowPosIndex; i < highPosIndex - 1; i++)
		{
			incrementSortRank();
			if(listToSort.get(i) <= pivotVal)
			{
				swap(listToSort, i, finalPosIndex);
				finalPosIndex++;
			}
			swap(listToSort, finalPosIndex, highPosIndex);
		}
		return finalPosIndex;
	}
	
	private int choosePivot(List<Integer> listToSort, int lowPosIndex, int highPosIndex)
	{
		int midPosIndex = (int) Math.floor((lowPosIndex + highPosIndex) / 2);
		int maxValIndex = 0;
		int minValIndex = 0;
		if(listToSort.get(lowPosIndex) > listToSort.get(midPosIndex))
		{
			maxValIndex = lowPosIndex;
			minValIndex = midPosIndex;
		}
		else
		{
			minValIndex = lowPosIndex;
			maxValIndex = midPosIndex;
		}
		
		if(listToSort.get(maxValIndex) < listToSort.get(highPosIndex))
		{
			return maxValIndex;
		}
		else if(listToSort.get(highPosIndex) > listToSort.get(minValIndex))
		{
			return highPosIndex;
		}
		else
		{
			return minValIndex;
		}		
	}


}
