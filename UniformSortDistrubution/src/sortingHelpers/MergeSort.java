package sortingHelpers;
import java.util.List;

public class MergeSort extends SortingMethods {
	
	public MergeSort()
	{
		super();
	}
	
	public MergeSort(int sortRank)
	{
		super(sortRank);
	}

	public List<Integer> Sort(List<Integer> listToSort)
	{
        this.listToSort = listToSort;
        helperList = this.GetListToSort();
        MergeSorting(0, this.listToSort.size() - 1);
        return GetListToSort();
    }
 
	/**
	 * Sort list using Merge Sort while counting number of comparisons
	 * @param none
	 * @return none
	 */
    private void MergeSorting(int lowerIndex, int higherIndex) {
         
        if (lowerIndex < higherIndex) {
            int middle = lowerIndex + (higherIndex - lowerIndex) / 2;
            MergeSorting(lowerIndex, middle);
            MergeSorting(middle + 1, higherIndex);
            MergeStep(lowerIndex, middle, higherIndex);
        }
    }
	/**
	 * Merge Step of Merge Sort where comparisons occur and are counted
	 * @param none
	 * @return none
	 */
    private void MergeStep(int lowerIndex, int middle, int higherIndex)
    {
        for (int i = lowerIndex; i <= higherIndex; i++)
        {
            helperList.set(i, new Integer(listToSort.get(i)));
        }
        int i = lowerIndex;
        int j = middle + 1;
        int k = lowerIndex;
        while (i <= middle && j <= higherIndex)
        {
        	incrementSortRank();
            if (helperList.get(i) <= helperList.get(j))
            {
                listToSort.set(k, helperList.get(i));
                i++;
            }
            else 
            {
                listToSort.set(k, helperList.get(j));
                j++;
            }
            k++;
        }
        while (i <= middle)
        {
            listToSort.set(k, helperList.get(i));
            k++;
            i++;
        }
    }
}
