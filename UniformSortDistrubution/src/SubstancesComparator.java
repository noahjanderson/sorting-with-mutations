import java.util.Comparator;


public class SubstancesComparator implements Comparator<Substances>
{
	@Override
	public int compare(Substances o1, Substances o2)
	{
		if(o1.value < o2.value)
		{
			return 1;
		}
		if(o1.value == o2.value)
		{
			return 0;
		}
		return -1;
	}

}
