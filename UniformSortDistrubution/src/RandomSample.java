import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import sortingHelpers.HeapSort;
import sortingHelpers.InsertionSort;
import sortingHelpers.IntroSort;
import sortingHelpers.MergeSort;
import sortingHelpers.QuickSort;
import sortingHelpers.RadixSort;

public class RandomSample {
	
	/**
	 * List of Intergers used to create a Random Uniformly Distributed Set
	 */
	private static List<Integer> listToRandomize;
	
	/**
	 * Constructor used to create List of Integers that are Uniformly Distibuted then Randomized
	 * @param int size
	 * @param int maxNumber
	 * @return List<Integers> clone/copy, not a reference
	 */
	public static List<Integer> RandomSample(int size, int maxNumber)
	{
		listToRandomize = new ArrayList<Integer>();		
		for(int i = 0; i < size; i++)
		{
			listToRandomize.add(new Integer(i + 1));
		}
	    RandomizeList();
	    return GetListToRandomize();
	}
	
	public static List<Integer> GetListToRandomize()
	{
		List<Integer> clone = new ArrayList<Integer>(listToRandomize.size());
	    for(Integer item: listToRandomize) clone.add(item);
	    return clone;
	}
	
	/**
	 * main
	 * @param String args
	 */
	public static void main(String[] args)
	{
		int m = 5000;
		int n = 5000;
		System.out.println(Arrays.toString(RandomSample(m, n).toArray()));
		System.out.println(String.format("Inversion Rank %d", RandomSample.GetListInversionRanking()));
		MergeSort ourMergeSort = new MergeSort();
//		System.out.println(String.format("\nAfter Merge Sort %s",
//				Arrays.toString(ourMergeSort.Sort(RandomSample.GetListToRandomize()).toArray())));
		ourMergeSort.Sort(RandomSample.GetListToRandomize());
		System.out.println(String.format("Merge Sort Rank %d", ourMergeSort.GetSortRank()));
		
		InsertionSort ourInsertSort = new InsertionSort();
//		System.out.println(String.format("\nAfter Insert Sort %s",
//				Arrays.toString(ourInsertSort.Sort(RandomSample.GetListToRandomize()).toArray())));
		ourInsertSort.Sort(RandomSample.GetListToRandomize());
		System.out.println(String.format("Insert Sort Rank %d", ourInsertSort.GetSortRank()));
		
		QuickSort ourQuickSort = new QuickSort();
//		System.out.println(String.format("\nAfter Quick Sort %s",
//				Arrays.toString(ourQuickSort.Sort(RandomSample.GetListToRandomize()).toArray())));
		ourQuickSort.Sort(RandomSample.GetListToRandomize());
		System.out.println(String.format("Quick Sort Rank %d", ourQuickSort.GetSortRank()));
		
//		HeapSort ourHeapSort = new HeapSort();
////		System.out.println(String.format("\nAfter Heap Sort %s",
////				Arrays.toString(ourHeapSort.Sort(RandomSample.GetListToRandomize()).toArray())));
//		ourHeapSort.Sort(RandomSample.GetListToRandomize());
//		System.out.println(String.format("Heap Sort Rank %d", ourHeapSort.GetSortRank()));
		
		
//		RadixSort ourRadixSort = new RadixSort();
//		System.out.println(String.format("\nAfter Radix Sort %s",
//				Arrays.toString(ourRadixSort.Sort(RandomSample.GetListToRandomize()).toArray())));
////		ourIntroSort.Sort(RandomSample.GetListToRandomize());
//		System.out.println(String.format("Radix Sort Rank %d", ourRadixSort.GetSortRank()));
		
//		IntroSort ourIntroSort = new IntroSort();
//		System.out.println(String.format("\nAfter Intro Sort %s",
//				Arrays.toString(ourIntroSort.Sort(RandomSample.GetListToRandomize()).toArray())));
////		ourIntroSort.Sort(RandomSample.GetListToRandomize());
//		System.out.println(String.format("Intro Sort Rank %d", ourIntroSort.GetSortRank()));


	}

	/**
	 * Randomize the Uniformly Distributed List
	 * @param none
	 * @return none
	 * @see RandomSample
	 */
	static void RandomizeList()
	{
		Random randItem = new Random();
		for(int i = 0; i < listToRandomize.size(); i++)
		{
			SwapListItems(i, randItem.nextInt(listToRandomize.size()));
		}
	}
	/**
	 * Swap 2 items in the list
	 * @param int pos1
	 * @param int pos2
	 */
	static void SwapListItems(int pos1, int pos2)
	{
		if(pos1 >= listToRandomize.size() || pos2 >= listToRandomize.size()) throw new IllegalArgumentException();
		else
		{
			Integer tempValuePos1 = new Integer(listToRandomize.get(pos1));		
			listToRandomize.set(pos1, listToRandomize.get(pos2));
			listToRandomize.set(pos2, tempValuePos1);
		}
	}
	/**
	 * Determine the Ranking of List by amount of its inversions
	 * @param none
	 * @return integer value of the ranking of inversions
	 */
	static int GetListInversionRanking()
	{
		int rank = 0;
		for (int i = 0; i < listToRandomize.size(); i++)
		{
			for(int j = i + 1; j < listToRandomize.size(); j++)
			{
				rank += listToRandomize.get(i) > listToRandomize.get(j) ? 1 : 0;
			}
		}
		return rank;
	}
}
