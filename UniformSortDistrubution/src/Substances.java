import java.util.Arrays;
import java.util.Comparator;


public class Substances
{
	String name;
	int value;
	
	public Substances()
	{
		name = null;
		value = 0;
	}
	
	public Substances(String name, int value)
	{
		this.name = name;
		this.value = value;
	}
}
